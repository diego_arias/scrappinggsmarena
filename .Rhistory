library(dplyr)  # Manipulación de datos
library(rvest)  # Scrapping
library(stringi) # Manipulación de caracteres
# Se crea función que extraiga los features
feat_ext_fun <- function(link){
# Se lee el link
web_aux <- read_html(link)
# Se obtiene el nombre del modelo
web_aux %>%
html_nodes('.specs-phone-name-title') %>%
html_text(.) -> modelo_aux
# Se obtienen los títulos
web_aux %>%
html_nodes('.ttl') %>%
html_text(.) %>%
data.frame() %>%
rename('title'='.') -> ttls_aux
# Se obtienen los specs
web_aux %>%
html_nodes('.nfo') %>%
html_text(.) %>%
data.frame() %>%
rename('desc'='.') -> specs_aux
# Se junta títulos y specs en un data frame
df_aux <- cbind(ttls_aux,specs_aux)
# Se crea una columna con el modelo del equipo
df_aux %>%
mutate(modelo = modelo_aux) %>%
select(modelo, title, desc) -> df_aux
return(df_aux)
}
links_href <- function(link, marca){
# Se lee link
web_aux <- read_html(link)
# Se obtienen los atributos de "a" y se guardan en una lista
web_aux %>%
html_nodes('a') %>%
html_attrs(.) -> atr_aux
# Se crea vector
href <- vector()
# Se concatenan todos los href con un ciclo for
for(j in 1:length(atr_aux)){
# Se extrae el i-ésimo href
aux  <- atr_aux[[j]]['href']
# Se convierte en vector para sólo dejar el contenido
aux  <- aux %>% as.vector()
# Se concatena con el vector de href
href <- c(href,aux)
# Se borra el campo auxiliar
rm(aux)
}
# Se limpian los links
href %>%
data.frame() %>%
rename('links' = '.') %>%
filter(grepl(marca,tolower(links))) %>%
filter(!(grepl('review',tolower(links)))) -> href
# Se agrega el link de la página para poder hacer la liga completa
href %>%
mutate(links = paste0('https://www.gsmarena.com/',links)) -> href
return(href)
}
marcas <- c('samsung', 'apple', 'huawei', 'nokia', 'sony', 'lg', 'htc', 'motorola', 'lenovo', 'xiaomi', 'google',
'asus', 'alcatel', 'zte', 'microsoft', 'lava', 'blu', 'acer', 'verykool')
url_1 <- c('https://www.gsmarena.com/samsung-phones-9.php',    'https://www.gsmarena.com/apple-phones-48.php',
'https://www.gsmarena.com/huawei-phones-58.php',    'https://www.gsmarena.com/nokia-phones-1.php',
'https://www.gsmarena.com/sony-phones-7.php',       'https://www.gsmarena.com/lg-phones-20.php',
'https://www.gsmarena.com/htc-phones-45.php',       'https://www.gsmarena.com/motorola-phones-4.php',
'https://www.gsmarena.com/lenovo-phones-73.php',    'https://www.gsmarena.com/xiaomi-phones-80.php',
'https://www.gsmarena.com/google-phones-107.php',   'https://www.gsmarena.com/asus-phones-46.php',
'https://www.gsmarena.com/alcatel-phones-5.php',    'https://www.gsmarena.com/zte-phones-62.php',
'https://www.gsmarena.com/microsoft-phones-64.php', 'https://www.gsmarena.com/lava-phones-94.php',
'https://www.gsmarena.com/blu-phones-67.php',       'https://www.gsmarena.com/acer-phones-59.php',
'https://www.gsmarena.com/verykool-phones-70.php')
url_raiz <- c('https://www.gsmarena.com/samsung-phones-f-9-0-p', 'https://www.gsmarena.com/apple-phones-f-48-0-p',
'https://www.gsmarena.com/huawei-phones-f-58-0-p', 'https://www.gsmarena.com/nokia-phones-f-1-0-p',
'https://www.gsmarena.com/sony-phones-f-7-0-p',    'https://www.gsmarena.com/lg-phones-f-20-0-p',
'https://www.gsmarena.com/htc-phones-f-45-0-p',    'https://www.gsmarena.com/motorola-phones-f-4-0-p',
'https://www.gsmarena.com/lenovo-phones-f-73-0-p', 'https://www.gsmarena.com/xiaomi-phones-f-80-0-p',
'',                                                'https://www.gsmarena.com/asus-phones-f-46-0-p',
'https://www.gsmarena.com/alcatel-phones-f-5-0-p', 'https://www.gsmarena.com/zte-phones-f-62-0-p',
'',                                                'https://www.gsmarena.com/lava-phones-f-94-0-p',
'https://www.gsmarena.com/blu-phones-f-67-0-p',    'https://www.gsmarena.com/acer-phones-f-59-0-p',
'https://www.gsmarena.com/verykool-phones-f-70-0-p')
df_marcas <- data.frame(marca    = marcas,
url_1    = url_1,
url_raiz = url_raiz)
features_marca <- function(marcad){
# Cascarón de df final
features_aux <- data.frame(marca  = character(),
modelo = character(),
title  = character(),
desc   = character())
# Se extrae la página original de la marca
url1 <- df_marcas %>% filter(marca == marcad) %>% select(url_1) %>% pull %>% as.character()
# Se lee la página
web_url1 <- read_html(url1)
# Se extrae el número máximo de páginas
web_url1 %>%
html_nodes('.pushT10') %>%
html_text(.) %>%
gsub('\r','',.) %>% gsub('\n','',.) %>% gsub('Pages:','',.) %>% gsub('Page:','',.) %>% trimws(.) %>%
gsub('  ', ' ',.) %>%
stri_split(str = ., regex = ' ') %>%
unlist() %>%
as.numeric() %>%
max() -> max_pag_aux
# Si el máximo número de páginas es NA, es porque el máximo número de páginas para esa marca es 1
if(is.na(max_pag_aux)){
max_pag_aux <- 1
}
# Se extraen los features de cada página
for(k in 1:max_pag_aux){
# Se extraen los links de las imágenes de cada página
if(k == 1){
url <- df_marcas %>% filter(marca == marcad) %>% select(url_1) %>% pull %>% as.character()
href <- links_href(url, marcad)} else {
url <- paste0(df_marcas %>% filter(marca == marcad) %>% select(url_raiz) %>% pull %>% as.character(),k,'.php')
href <- links_href(url, marcad)
}
# Se extraen los features de cada link
for(w in 1:nrow(href)){
# Se aplica la función a cada link para obtener los features
df_link <- feat_ext_fun(href$links[w])
# Descanso para el systema
Sys.sleep(1)
# Cerrar conexiones
closeAllConnections()
# Se maximiza tiempo de conexión
options(timeout= 4000000)
# Se hace un condicional, si el largo de la tabla es mayor que cero, se adiciona al df
if(nrow(df_link)>0){
# Se agrega la marca al df
df_link %>%
mutate(marca = marcad) %>%
select(marca,
modelo,
title,
desc) -> df_link
features_aux <- rbind(features_aux,df_link)
# Se imprime el número de hoja y el número de link
print(paste0(marcad, ' - paginas: ',k,'-',max_pag_aux, ' links: ', w, '-', nrow(href)))
}
}
}
return(features_aux)
}
features <- data.frame(marca  = character(),
modelo = character(),
title  = character(),
desc   = character())
for(r in 1:nrow(df_marcas)){
aux <- features_marca(df_marcas$marca[r])
features <- rbind(features,aux)
}
